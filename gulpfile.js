var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');
var merge = require('merge-stream');

gulp.task('install', function() {

    gulp.src('less/paddock.less')
        .pipe(less({style: 'expanded'}))
        .pipe(gulp.dest('assets'));

});